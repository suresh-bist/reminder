class ApiRequest < ApplicationRecord
	belongs_to :user_request
	scope :uber, lambda { where(provider: Cons::UBER) }
	scope :google, lambda { where(provider: Cons::GOOGLE) }
end
