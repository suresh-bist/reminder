class UserRequest < ApplicationRecord
	validates :email, presence: true, format: /@/
	validates :source_latitude, :source_longitude, :destination_latitude, :destination_longitude, :reach_time, presence: :true

	has_many :api_requests

	after_create_commit :analyse_request

	def self.register(params)
		email = params[:email].to_s
		source_latitude = params[:source_latitude].to_f
		source_longitude = params[:source_longitude].to_f
		destination_latitude = params[:destination_latitude].to_f
		destination_longitude = params[:destination_longitude].to_f
		reach_time = Time.zone.parse(params[:reach_time].to_s)
		user_request = UserRequest.create({
				email: email,
				source_latitude: source_latitude,
				source_longitude: source_longitude,
				destination_latitude: destination_latitude,
				destination_longitude: destination_longitude,
				reach_time: reach_time
			})
		user_request
	end

	def analyse_request
		current_time, user_reach_time = Time.zone.now.to_i, self.reach_time.to_i
		google_duration_estimate = self.get_google_estimate(self.id)
		return send_failure_email(self.id) if google_duration_estimate.nil?
		plausible_estimated_start_time = user_reach_time - google_duration_estimate
		return send_failure_email(self.id) if plausible_estimated_start_time < current_time 
		estimated_time_with_delay = plausible_estimated_start_time - Cons::MAX_DELAY
		if estimated_time_with_delay < current_time
			GenericWorker.perform_async(self.id) 
		else 
			delay_time = estimated_time_with_delay/60
			GenericWorker.perform_in(delay_time.minutes, self.id)
		end
	end

	def process_request(user_request_id)
    user_request = UserRequest.find_by_id(user_request_id)
    return user_request.send_failure_email(user_request.id) if user_request.api_requests.google.pluck(:estimated_time).count(nil) > 3 or user_request.api_requests.uber.pluck(:estimated_time).count(nil) > 3
		user_reach_time = user_request.reach_time.to_i
		uber_eta, is_uber_available = get_uber_estimate_and_availability(user_request.id)
		if is_uber_available
			current_time = Time.zone.now.to_i 
			google_duration_estimate = user_request.get_google_estimate(user_request.id)
			total_travel_time = google_duration_estimate + uber_eta
			
			if (total_travel_time + current_time) == user_reach_time
				return user_request.send_success_email(user_request.id)
			end

			deviation_time_in_minutes = (user_reach_time - (total_travel_time + current_time))/60

			if (deviation_time_in_minutes >= 0 and deviation_time_in_minutes <= Cons::BUFFER_TIME)
		 		EmailWorker.perform_in(deviation_time_in_minutes.minutes, user_request.id)
			elsif deviation_time_in_minutes >= 0 
				GenericWorker.perform_in(deviation_time_in_minutes.minutes, user_request.id)
			else
				user_request.send_failure_email(user_request.id)
			end
		else
			GenericWorker.perform_in(1.minutes, user_request.id)
		end
	end

	def send_failure_email(user_request_id)
		user_request = UserRequest.find_by_id(user_request_id)
		subject = I18n.t("failure_reminder_to_requestor.template_subject")
		salutation = I18n.t("failure_reminder_to_requestor.template_salutation")
		body = I18n.t("failure_reminder_to_requestor.template_body")
		footer = I18n.t("failure_reminder_to_requestor.template_footer")
		to = user_request.email
		GenericMailer.send_mail(to, subject, salutation, body, footer).deliver_later
	end

	def send_success_email(user_request_id)
		user_request = UserRequest.find_by_id(user_request_id)
		subject = I18n.t("success_reminder_to_requestor.template_subject")
		salutation = I18n.t("success_reminder_to_requestor.template_salutation")
		body = I18n.t("success_reminder_to_requestor.template_body")
		footer = I18n.t("success_reminder_to_requestor.template_footer")
		to = user_request.email
		GenericMailer.send_mail(to, subject, salutation, body, footer).deliver_now
	end

	def get_google_estimate(user_request_id)
		user_request = UserRequest.find_by_id(user_request_id)
		google_api = Cons::GOOGLE_DISTANCE_MATRIX_API
		google_api.gsub!('<source_coordinates>', "#{user_request.source_latitude},#{user_request.source_longitude}")
		google_api.gsub!('<destination_coordinates>', "#{user_request.destination_latitude},#{user_request.destination_longitude}")
		google_api.gsub!('<reach_time>', "#{user_request.reach_time.to_i}")
		request = Typhoeus.get(google_api, followlocation: true)
	  response, estimated_time = nil, nil
		if request.success?
			response = JSON.parse(request.body)
  		estimated_time = transform_google_response(response)
		end
  	ApiRequest.create(user_request_id: user_request.id, url: google_api.to_s, provider: Cons::GOOGLE, response: response, estimated_time: estimated_time)
  	estimated_time
	end

	def get_uber_estimate_and_availability(user_request_id)
		user_request = UserRequest.find_by_id(user_request_id)
		uber_api = Cons::UBER_ESTIMATE_API
		uber_api.gsub!('<latitude>', "#{user_request.source_latitude}")
		uber_api.gsub!('<longitude>', "#{user_request.source_longitude}")
		request = Typhoeus.get(uber_api, followlocation: true)
	  response, estimated_time = nil, nil
	  availability = false
		if request.success?
			response = JSON.parse(request.body)
  		estimated_time, availability = transform_uber_response(response)
		end
  	ApiRequest.create(user_request_id: user_request.id, url: uber_api.to_s, provider: Cons::UBER, response: response, estimated_time: estimated_time)
  	[estimated_time,availability]
	end

	def transform_google_response response
		response['rows'][0]['elements'][0]['duration']['value'].to_i rescue nil
	end

	def transform_uber_response response 
		availability = false 
		estimated_time = nil
		response['times'].each do |res|
			if res['localized_display_name'] == Cons::UBER_GO
				availability = true
				estimated_time = res['estimate'].to_i
			end
		end
		[estimated_time, availability]
	end

end
