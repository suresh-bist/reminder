class EmailWorker 
	include Sidekiq::Worker
	sidekiq_options :retry => 5, :dead => false
	

  def perform(user_request_id)
    user_request = UserRequest.find_by_id(user_request_id.to_i)
    user_request.send_success_email(user_request.id)
  end

end
