class UserRequestController < ApplicationController
	def index
	end

	def create
		user_request = UserRequest.register(user_request_params)
		if user_request.persisted?
			flash[:notice] = I18n.t("user_request_success_submit.body")
		else
			flash[:alert] = I18n.t("user_request_failure_submit.body")
		end
		redirect_to root_path
	end

	private
	def user_request_params
		params.require(:user_request).permit(:email, :source_latitude, :source_longitude, :destination_latitude, :destination_longitude, :reach_time)
	end
end
