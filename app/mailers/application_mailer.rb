class ApplicationMailer < ActionMailer::Base
  default from: 'transportermailernotifier@gmail.com '
  layout 'mailer'
end
