class GenericMailer < ApplicationMailer
	layout 'mailer'
	default from: 'transportermailernotifier@gmail.com'

    def send_mail(to, subject, salutation, body, footer)
    	@salutation = salutation 
    	@body = body
    	@footer = footer
      mail(:to => to, :subject => subject, :date => Date.today)
    end
    
'''
To send a success/failure email do this 
subject = I18n.t("failure_reminder_to_requestor.template_subject")
salutation = I18n.t("failure_reminder_to_requestor.template_salutation")
body = I18n.t("failure_reminder_to_requestor.template_body")
footer = I18n.t("failure_reminder_to_requestor.template_footer")
to = "bist.suresh@ymail.com"
GenericMailer.send_mail(to, subject, salutation, body, footer).deliver_now
'''
end
