class CreateUserRequests < ActiveRecord::Migration[5.2]
  def change
    create_table :user_requests do |t|
			t.string :email
			t.float :source_latitude
			t.float :source_longitude
			t.float :destination_latitude
			t.float :destination_longitude
			t.datetime :reach_time
      t.timestamps
      t.timestamps
    end
  end
end
