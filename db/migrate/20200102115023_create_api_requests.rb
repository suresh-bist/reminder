class CreateApiRequests < ActiveRecord::Migration[5.2]
  def change
    create_table :api_requests do |t|
    	t.belongs_to :user_request
    	t.string :url
    	t.string :provider
    	t.json :response 
    	t.integer :estimated_time
      t.timestamps
    end
  end
end
