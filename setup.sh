#!/bin/bash

GREEN='\033[0;32m'
RED='\033[0;31m'
NC='\033[0m'
BLUE=$'\e[1;34m'

source ./service_config.sh

rvm_installation(){
	printf "${GREEN}RVM installation...... ${NC}\n"
	curl -sSL https://rvm.io/pkuczynski.asc | gpg --import -
	gpg --keyserver hkp://pool.sks-keyservers.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3 7D2BAF1CF37B13E2069D6956105BD0E739499BDB
	curl -sSL https://get.rvm.io | bash -s stable --ruby
	source $HOME/.rvm/scripts/rvm
	rvm install ruby-2.5.3
	rvm use ruby-2.5.3 --default
	echo '[[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm" # Load RVM into a shell session *as a function*' >> ~/.bash_profile
	printf "${GREEN}RVM installation complete!! ...... \n"
}

redis_installation(){
	printf "${GREEN}Redis installation...... ${NC}\n"
	brew install redis
	if ! wget redis-cli ping 2>&1 | grep -q "pong"; then
		brew services restart redis
		ln -sfv /usr/local/opt/redis/*.plist ~/Library/LaunchAgents #launch redis on system boot
		printf "${GREEN}Redis installation complete!!...... ${NC}\n"
  else
    printf "${RED} Redis installation failed. ${NC}\n"
  fi
}

postgres_installation(){
	printf "${GREEN}Postgresql installation...... ${NC}\n"
	cd ~/
	/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
	brew install postgresql
	pg_ctl -D /usr/local/var/postgres start && brew services start postgresql
	printf "${GREEN}Postgresql installation complete!!...... ${NC}\n"
	printf "${BLUE} Please execute this command to open console - 'psql postgres' ${NC} \n"
}

bundle_app(){
	bundle install 
}

run_migration(){
	bundle exec rake db:drop
	bundle exec rake db:setup
}

install_components(){
	printf "${GREEN}Starting installation of necessary components...... ${NC}\n"
	rvm_installation
	redis_installation
	postgres_installation
	bundle_app
	run_migration 
	printf "${GREEN}Necessary components installed...... \n If you notice any errors please make sure you fix before proceeding. ${NC}\n"
}

install_components