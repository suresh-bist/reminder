# README

Uber Reminder

### Ruby
* Object oriented, dynamically typed and expressive syntax
* Single threaded execution model. Run with multi-process configuration to achieve concurrent behavior
* Large eco-system of libraries and frameworks
* Provides rapid-application development capabilities

### Ruby On Rails 
* A web-application framework that includes everything needed to create database-backed web applications according to the Model-View-Controller (MVC)

### PostgresSql 
* RDBMS, Modern, high-performance, ACID compliant
* This is our primary data-store for all customer data
* Supports modern facilities like CTEs, programmability
* Supports high-availability features like replications, backups, clustering capabilities
* Used primarily via Amazon Relational Database Service (RDS)

### Sidekiq
* Sidekiq is an open source job scheduler written in Ruby.
* Simple, efficient background processing for Ruby

### Installation for macOS
* run script `setup.sh`
(Haven't tested the script)

## Algorithm 
	user_reach_time = User entered time 
	plausible_estimated_start_time = user_reach_time - google_duration_estimate
	total_travel_time = Google Estimated + Uber ETA
	buffer_time = (constant) 5 minutes
	max_delay = (constant) 60 minutes
	estimated_time_with_delay = plausible_estimated_start_time - max_delay

	if estimated_time_with_delay < current_time
		go to step 1 
	else 
		go to step 1 estimated_time_with_delay minutes 
	end
	
	1. if (total_travel_time + current_time) == user_reach_time
		send success reminder email 
	2. calculate deviation_time_in_minutes
			deviation_time_in_minutes = (user_reach_time - (total_travel_time + current_time))/60
 	3.  if deviation_time_in_minutes.positive? and deviation_time_in_minutes <= Cons::BUFFER_TIME
			schedule success reminder email in deviation_time_in_minutes
		else 
			Go to step 1 after deviation_time_in_minutes