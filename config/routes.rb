Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 	'user_request#index'
  get 	'user_request/index' => 'user_request#index'
  post 	'user_request/create' => 'user_request#create'
end
